﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Renegade.Entities
{
    public partial class PurchaseOrderDetail
    {
        public Guid PurchaseOrderDetailId { get; set; }
        public Guid PurchaseOrderId { get; set; }
        public Guid VideoGameKeyId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        [InverseProperty("PurchaseOrderDetail")]
        public virtual Account CreatedByNavigation { get; set; }
        [ForeignKey("PurchaseOrderId")]
        [InverseProperty("PurchaseOrderDetail")]
        public virtual PurchaseOrder PurchaseOrder { get; set; }
        [ForeignKey("VideoGameKeyId")]
        [InverseProperty("PurchaseOrderDetail")]
        public virtual VideoGame VideoGameKey { get; set; }
    }
}
