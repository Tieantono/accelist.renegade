﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Renegade.Entities
{
    public partial class VideoGameScreenshot
    {
        public Guid VideoGameId { get; set; }
        public Guid BlobId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        [InverseProperty("VideoGameScreenshotCreatedByNavigation")]
        public virtual Account CreatedByNavigation { get; set; }
        [ForeignKey("UpdatedBy")]
        [InverseProperty("VideoGameScreenshotUpdatedByNavigation")]
        public virtual Account UpdatedByNavigation { get; set; }
    }
}
