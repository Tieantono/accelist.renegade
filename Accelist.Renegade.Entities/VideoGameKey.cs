﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Renegade.Entities
{
    public partial class VideoGameKey
    {
        public Guid VideoGameKeyId { get; set; }
        public Guid? VideoGameId { get; set; }
        public DateTimeOffset SoldDate { get; set; }
        public DateTimeOffset RedeemedDate { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        [InverseProperty("VideoGameKey")]
        public virtual Account CreatedByNavigation { get; set; }
        [ForeignKey("VideoGameId")]
        [InverseProperty("VideoGameKey")]
        public virtual VideoGame VideoGame { get; set; }
    }
}
