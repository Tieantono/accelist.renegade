﻿INSERT INTO Account(Username, [Password], IsAdmin)
VALUES('admin@accelist.com', '$2a$10$UU./eSSaj5ueteubIP3fvOPPJ76kDyy91p5axXdSrqvLjLQiAISga', 1)

INSERT INTO Account(Username, [Password], IsAdmin)
VALUES('user@accelist.com', '$2a$10$UU./eSSaj5ueteubIP3fvOPPJ76kDyy91p5axXdSrqvLjLQiAISga', 0)