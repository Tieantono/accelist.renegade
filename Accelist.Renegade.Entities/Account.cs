﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Renegade.Entities
{
    public partial class Account
    {
        public Account()
        {
            PurchaseOrderCreatedByNavigation = new HashSet<PurchaseOrder>();
            PurchaseOrderDetail = new HashSet<PurchaseOrderDetail>();
            PurchaseOrderUsernameNavigation = new HashSet<PurchaseOrder>();
            VideoGameCreatedByNavigation = new HashSet<VideoGame>();
            VideoGameKey = new HashSet<VideoGameKey>();
            VideoGameScreenshotCreatedByNavigation = new HashSet<VideoGameScreenshot>();
            VideoGameScreenshotUpdatedByNavigation = new HashSet<VideoGameScreenshot>();
            VideoGameUpdatedByNavigation = new HashSet<VideoGame>();
        }

        [Key]
        [StringLength(255)]
        public string Username { get; set; }
        [Required]
        [StringLength(128)]
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }

        [InverseProperty("CreatedByNavigation")]
        public virtual ICollection<PurchaseOrder> PurchaseOrderCreatedByNavigation { get; set; }
        [InverseProperty("CreatedByNavigation")]
        public virtual ICollection<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }
        [InverseProperty("UsernameNavigation")]
        public virtual ICollection<PurchaseOrder> PurchaseOrderUsernameNavigation { get; set; }
        [InverseProperty("CreatedByNavigation")]
        public virtual ICollection<VideoGame> VideoGameCreatedByNavigation { get; set; }
        [InverseProperty("CreatedByNavigation")]
        public virtual ICollection<VideoGameKey> VideoGameKey { get; set; }
        [InverseProperty("CreatedByNavigation")]
        public virtual ICollection<VideoGameScreenshot> VideoGameScreenshotCreatedByNavigation { get; set; }
        [InverseProperty("UpdatedByNavigation")]
        public virtual ICollection<VideoGameScreenshot> VideoGameScreenshotUpdatedByNavigation { get; set; }
        [InverseProperty("UpdatedByNavigation")]
        public virtual ICollection<VideoGame> VideoGameUpdatedByNavigation { get; set; }
    }
}
