﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace Accelist.Renegade.Entities
{
    public partial class RenegadeDbContext : DbContext
    {
        public RenegadeDbContext(DbContextOptions<RenegadeDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Blob> Blob { get; set; }
        public virtual DbSet<PurchaseOrder> PurchaseOrder { get; set; }
        public virtual DbSet<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }
        public virtual DbSet<VideoGame> VideoGame { get; set; }
        public virtual DbSet<VideoGameKey> VideoGameKey { get; set; }
        public virtual DbSet<VideoGameScreenshot> VideoGameScreenshot { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.Username)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.CreatedBy)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('SYSTEM')");

                entity.Property(e => e.Password).IsUnicode(false);
            });

            modelBuilder.Entity<Blob>(entity =>
            {
                entity.Property(e => e.BlobId).ValueGeneratedNever();

                entity.Property(e => e.ContentType).IsUnicode(false);

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(sysdatetimeoffset())");
            });

            modelBuilder.Entity<PurchaseOrder>(entity =>
            {
                entity.Property(e => e.PurchaseOrderId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Username).IsUnicode(false);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.PurchaseOrderCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseOrder_Create_Account");

                entity.HasOne(d => d.UsernameNavigation)
                    .WithMany(p => p.PurchaseOrderUsernameNavigation)
                    .HasForeignKey(d => d.Username)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseOrder_Account");
            });

            modelBuilder.Entity<PurchaseOrderDetail>(entity =>
            {
                entity.HasIndex(e => new { e.PurchaseOrderId, e.VideoGameKeyId })
                    .HasName("UQ_PurchaseOrderDetail_PurchaseKey")
                    .IsUnique();

                entity.Property(e => e.PurchaseOrderDetailId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.PurchaseOrderDetail)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseOrderDetail_Create_Account");

                entity.HasOne(d => d.PurchaseOrder)
                    .WithMany(p => p.PurchaseOrderDetail)
                    .HasForeignKey(d => d.PurchaseOrderId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseOrderDetail_PurchaseOrder");

                entity.HasOne(d => d.VideoGameKey)
                    .WithMany(p => p.PurchaseOrderDetail)
                    .HasForeignKey(d => d.VideoGameKeyId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_PurchaseOrderDetail_VideoGameKey");
            });

            modelBuilder.Entity<VideoGame>(entity =>
            {
                entity.Property(e => e.VideoGameId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.Description).IsUnicode(false);

                entity.Property(e => e.Name).IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.UpdatedBy).IsUnicode(false);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.VideoGameCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VideoGame_Create_Account");

                entity.HasOne(d => d.PromotionalImageNavigation)
                    .WithMany(p => p.VideoGame)
                    .HasForeignKey(d => d.PromotionalImage)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VideoGame_Blob");

                entity.HasOne(d => d.UpdatedByNavigation)
                    .WithMany(p => p.VideoGameUpdatedByNavigation)
                    .HasForeignKey(d => d.UpdatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VideoGame_Update_Account");
            });

            modelBuilder.Entity<VideoGameKey>(entity =>
            {
                entity.Property(e => e.VideoGameKeyId).ValueGeneratedNever();

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.VideoGameKey)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VideoGameKey_Account");

                entity.HasOne(d => d.VideoGame)
                    .WithMany(p => p.VideoGameKey)
                    .HasForeignKey(d => d.VideoGameId)
                    .HasConstraintName("FK_VideoGameKey_VideoGame");
            });

            modelBuilder.Entity<VideoGameScreenshot>(entity =>
            {
                entity.HasKey(e => new { e.VideoGameId, e.BlobId });

                entity.Property(e => e.CreatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.CreatedBy).IsUnicode(false);

                entity.Property(e => e.UpdatedAt).HasDefaultValueSql("(sysdatetimeoffset())");

                entity.Property(e => e.UpdatedBy).IsUnicode(false);

                entity.HasOne(d => d.CreatedByNavigation)
                    .WithMany(p => p.VideoGameScreenshotCreatedByNavigation)
                    .HasForeignKey(d => d.CreatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VideoGameScreenshot_Create_Account");

                entity.HasOne(d => d.UpdatedByNavigation)
                    .WithMany(p => p.VideoGameScreenshotUpdatedByNavigation)
                    .HasForeignKey(d => d.UpdatedBy)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_VideoGameScreenshot_Update_Account");
            });
        }
    }
}
