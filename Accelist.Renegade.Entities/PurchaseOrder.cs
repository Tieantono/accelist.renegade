﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Renegade.Entities
{
    public partial class PurchaseOrder
    {
        public PurchaseOrder()
        {
            PurchaseOrderDetail = new HashSet<PurchaseOrderDetail>();
        }

        public Guid PurchaseOrderId { get; set; }
        [Required]
        [StringLength(255)]
        public string Username { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        [InverseProperty("PurchaseOrderCreatedByNavigation")]
        public virtual Account CreatedByNavigation { get; set; }
        [ForeignKey("Username")]
        [InverseProperty("PurchaseOrderUsernameNavigation")]
        public virtual Account UsernameNavigation { get; set; }
        [InverseProperty("PurchaseOrder")]
        public virtual ICollection<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }
    }
}
