﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Renegade.Entities
{
    public partial class VideoGame
    {
        public VideoGame()
        {
            PurchaseOrderDetail = new HashSet<PurchaseOrderDetail>();
            VideoGameKey = new HashSet<VideoGameKey>();
        }

        public Guid VideoGameId { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(4000)]
        public string Description { get; set; }
        [Column(TypeName = "decimal(18, 0)")]
        public decimal Price { get; set; }
        public DateTimeOffset ReleaseDate { get; set; }
        public Guid PromotionalImage { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string CreatedBy { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string UpdatedBy { get; set; }

        [ForeignKey("CreatedBy")]
        [InverseProperty("VideoGameCreatedByNavigation")]
        public virtual Account CreatedByNavigation { get; set; }
        [ForeignKey("PromotionalImage")]
        [InverseProperty("VideoGame")]
        public virtual Blob PromotionalImageNavigation { get; set; }
        [ForeignKey("UpdatedBy")]
        [InverseProperty("VideoGameUpdatedByNavigation")]
        public virtual Account UpdatedByNavigation { get; set; }
        [InverseProperty("VideoGameKey")]
        public virtual ICollection<PurchaseOrderDetail> PurchaseOrderDetail { get; set; }
        [InverseProperty("VideoGame")]
        public virtual ICollection<VideoGameKey> VideoGameKey { get; set; }
    }
}
