﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Accelist.Renegade.Entities
{
    public partial class Blob
    {
        public Blob()
        {
            VideoGame = new HashSet<VideoGame>();
        }

        public Guid BlobId { get; set; }
        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset UpdatedAt { get; set; }
        [Required]
        [StringLength(255)]
        public string Name { get; set; }
        [Required]
        [StringLength(255)]
        public string ContentType { get; set; }

        [InverseProperty("PromotionalImageNavigation")]
        public virtual ICollection<VideoGame> VideoGame { get; set; }
    }
}
