﻿using System;

namespace Accelist.Renegade.Services.Models.Stores
{
    public class HighlightVideoGameModel
    {
        public Guid VideoGameId { get; set; }

        public Guid Name { get; set; }

        public string ShortDescription { get; set; }

        public string ImageName { get; set; }

        public decimal Price { get; set; }
    }
}
