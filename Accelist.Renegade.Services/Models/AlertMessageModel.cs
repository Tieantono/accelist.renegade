﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Accelist.Renegade.Services.Models
{
    public class AlertMessageModel
    {
        public string Message { get; set; }

        public string Category { get; set; }
    }
}
