﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Accelist.Renegade.Services.Models.Libraries
{
    public class UserGameModel
    {
        public Guid VideoGameid { get; set; }

        public Guid BlobId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }
    }
}
