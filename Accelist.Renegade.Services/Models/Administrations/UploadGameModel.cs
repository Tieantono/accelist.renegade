﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Accelist.Renegade.Services.Models.Administrations
{
    public class UploadGameModel
    {
        [Required]
        [StringLength(255)]
        public string Name { get; set; }

        [Required]
        [StringLength(4_000)]
        public string Description { get; set; }

        [Required]
        [Range(0, 1_000_000_000)]
        public decimal Price { get; set; }
        
        [Required]
        [Display(Name = "Banner Image")]
        public IFormFile File { get; set; }
    }
}
