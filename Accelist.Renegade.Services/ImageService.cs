﻿using Accelist.Renegade.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Minio;
using Minio.DataModel;

namespace Accelist.Renegade.Services
{
    public class ImageService
    {
        private readonly RenegadeDbContext DB;
        private readonly MinioClient Minio;

        public ImageService(RenegadeDbContext db, MinioClient minio)
        {
            this.DB = db;
            this.Minio = minio;
        }
        
        public async Task<string> GetMiniOImage(Guid id)
        {
            var bucketName = "renegade";
            var location = "us-east-1";
            var objectName = id.ToString();

            // Make a bucket on the server, if not already present.
            bool found = await this.Minio.BucketExistsAsync(bucketName);
            if (!found)
            {
                await this.Minio.MakeBucketAsync(bucketName, location);
            }

            var expiry = (int)TimeSpan.FromMinutes(15).TotalSeconds;
            
            var url = await this.Minio.PresignedGetObjectAsync("renegade", id.ToString(), expiry);
            
            return url;
        }
    }
}
