﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Accelist.Renegade.Services.Constants
{
    public class AlertCategories
    {
        public const string Success = "success";

        public const string Danger = "danger";

        public const string Info = "info";
    }
}
