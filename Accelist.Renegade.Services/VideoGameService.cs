﻿using Accelist.Renegade.Entities;
using Accelist.Renegade.Services.Models.Administrations;
using Accelist.Renegade.Services.Models.Libraries;
using Microsoft.EntityFrameworkCore;
using Minio;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Accelist.Renegade.Services
{
    public class VideoGameService
    {
        private readonly RenegadeDbContext DB;
        private readonly MinioClient Minio;

        public VideoGameService(RenegadeDbContext db, MinioClient minio)
        {
            this.DB = db;
            this.Minio = minio;
        }

        public async Task CreateVideoGame(UploadGameModel uploadData, string username)
        {
            var imageBytes = new byte[0];
            var currentDate = DateTimeOffset.Now;
            using (var ms = new MemoryStream())
            {
                using (var stream = uploadData.File.OpenReadStream())
                {
                    await stream.CopyToAsync(ms);
                }

                imageBytes = ms.ToArray();
            }

            var blobId = Guid.NewGuid();
            
            var bucketName = "renegade";
            var location = "us-east-1";
            var objectName = blobId.ToString();
            var contentType = uploadData.File.ContentType;

            // Make a bucket on the server, if not already present.
            bool found = await this.Minio.BucketExistsAsync(bucketName);
            if (!found)
            {
                await this.Minio.MakeBucketAsync(bucketName, location);
            }
            // Upload a file to bucket.
            using (var stream = uploadData.File.OpenReadStream())
            {
                await this.Minio.PutObjectAsync(bucketName, objectName, stream, stream.Length, contentType);
            }
            
            this.DB.Blob.Add(new Blob
            {
                BlobId = blobId,
                Name = uploadData.Name,
                ContentType = uploadData.File.ContentType,
                CreatedAt = currentDate,
                UpdatedAt = currentDate
            });

            this.DB.VideoGame.Add(new VideoGame
            {
                VideoGameId = Guid.NewGuid(),
                Name = uploadData.Name,
                Description = uploadData.Description,
                Price = uploadData.Price,
                CreatedAt = currentDate,
                CreatedBy = username,
                UpdatedAt = currentDate,
                UpdatedBy = username,
                PromotionalImage = blobId,
                ReleaseDate = currentDate
            });

            this.DB.SaveChanges();
        }

        public async Task<List<UserGameModel>> GetUserGames(string username)
        {
            var games = await this
                .DB
                .VideoGame
                // Only for temporary, since there is no purchase UI yet.
                .Where(Q => Q.CreatedBy == username)
                .Select(Q => new UserGameModel
                {
                    VideoGameid = Q.VideoGameId,
                    BlobId = Q.PromotionalImage,
                    Name = Q.Name,
                    Description = Q.Description
                })
                .ToListAsync();

            return games;
        }
    }
}
