﻿using Accelist.Renegade.Entities;
using Accelist.Renegade.Services.Constants;
using Microsoft.AspNetCore.Authentication;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Accelist.Renegade.Services
{
    public class AuthenticationService
    {
        private readonly RenegadeDbContext DB;

        public AuthenticationService(RenegadeDbContext db)
        {
            this.DB = db;
        }
        
        /// <summary>
        /// Used for storing invalid username or password error message.
        /// </summary>
        private string InvalidUsernameOrPasswordMessage => "Username/password is invalid";

        /// <summary>
        /// Verify the login data based on the user's inputs.
        /// </summary>
        /// <param name="username">The submitted username.</param>
        /// <param name="password">The submitted raw password.</param>
        /// <returns>Empty string if login data is valid, otherwise return an error message.</returns>
        public async Task<string> VerifyLoginData(string username, string password)
        {
            var storedPassword = await this
                .DB
                .Account
                .Where(Q => Q.Username == username)
                .Select(Q => Q.Password)
                .FirstOrDefaultAsync();

            if (string.IsNullOrEmpty(storedPassword) == true)
            {
                return this.InvalidUsernameOrPasswordMessage;
            }

            var isValid = BCrypt.Net.BCrypt.Verify(password, storedPassword);

            if (isValid == false)
            {
                return this.InvalidUsernameOrPasswordMessage;
            }

            return string.Empty;
        }
        
        /// <summary>
        /// Build claims for cookie authentication.
        /// </summary>
        /// <param name="username">The submitted username.</param>
        /// <returns><seealso cref="ClaimsIdentity"/> object.</returns>
        public async Task<ClaimsIdentity> BuildClaimsIdentity(string username)
        {
            var isAdmin = await this
                .DB
                .Account
                .AnyAsync(Q => Q.Username == username && Q.IsAdmin == true);

            var role = UserRoles.User;

            if (isAdmin == true)
            {
                role = UserRoles.Admin;
            }

            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, username),
                new Claim(ClaimTypes.Role, role)
            };

            var claimsIdentity = new ClaimsIdentity(claims, Authentications.AuthenticationScheme);

            return claimsIdentity;
        }
    }
}
