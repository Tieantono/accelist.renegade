﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accelist.Renegade.Services;
using Accelist.Renegade.Services.Constants;
using Accelist.Renegade.Services.Models;
using Accelist.Renegade.Services.Models.Administrations;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.Renegade.Web.Pages.Administrations.UploadGame
{
    [Authorize(Roles = UserRoles.Admin)]
    public class IndexModel : PageModel
    {
        private readonly VideoGameService VideoGameService;

        public IndexModel(VideoGameService videoGameService)
        {
            this.VideoGameService = videoGameService;
        }
        
        [BindProperty]
        public UploadGameModel UploadGameForm { get; set; }

        public void OnGet()
        {

        }

        public async Task<IActionResult> OnPostAsync()
        {
            var username = this
                .User
                .Claims
                .Where(Q => Q.Type == ClaimTypes.Name)
                .Select(Q => Q.Value)
                .FirstOrDefault();

            await this.VideoGameService.CreateVideoGame(this.UploadGameForm, username);

            this.ViewData[ViewDataVariables.Alert] = new AlertMessageModel
            {
                Message = "Successfully uploaded a new game",
                Category = AlertCategories.Success
            };

            return RedirectToPage("/Index");
        }
    }
}