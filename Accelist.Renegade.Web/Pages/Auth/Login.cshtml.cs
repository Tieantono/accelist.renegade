﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accelist.Renegade.Services;
using Accelist.Renegade.Services.Constants;
using Accelist.Renegade.Services.Models;
using Accelist.Renegade.Services.Models.Authentications;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.Renegade.Web.Pages.Auth
{
    public class LoginModel : PageModel
    {
        private readonly Services.AuthenticationService AuthenticationService;

        public LoginModel(Services.AuthenticationService authenticationService)
        {
            this.AuthenticationService = authenticationService;
        }

        [BindProperty]
        public LoginFormModel LoginForm { get; set; }
        
        public IActionResult OnGet()
        {
            if (this.User.Identity.IsAuthenticated == true)
            {
                return RedirectToPage("/Index");
            }

            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var validationResultMessage = await this
                .AuthenticationService
                .VerifyLoginData(this.LoginForm.Username, this.LoginForm.Password);

            if (string.IsNullOrEmpty(validationResultMessage) == false)
            {
                this.ViewData[ViewDataVariables.Alert] = new AlertMessageModel
                {
                    Message = validationResultMessage,
                    Category = AlertCategories.Danger
                };

                return Page();
            }

            var authProperties = new AuthenticationProperties
            {
                IsPersistent = this.LoginForm.IsPersistent
            };

            var claimsIdentity = await this.AuthenticationService.BuildClaimsIdentity(this.LoginForm.Username);

            var claimsPrincipal = new ClaimsPrincipal(claimsIdentity);

            await this.HttpContext.SignInAsync(Authentications.AuthenticationScheme, claimsPrincipal, authProperties);
            
            this.ViewData[ViewDataVariables.Alert] = new AlertMessageModel
            {
                Message = "You have successfully login into Renegade store",
                Category = AlertCategories.Success
            };

            return RedirectToPage("/Index");
        }
    }
}