﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Accelist.Renegade.Services.Constants;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace Accelist.Renegade.Web.Pages.Auth
{
    public class LogoutModel : PageModel
    {
        public IActionResult OnGet()
        {
            if (this.User.Identity.IsAuthenticated == true)
            {
                return RedirectToPage("/Index");
            }

            return Page();
        }

        public async Task<IActionResult> OnPost()
        {
            await this.HttpContext.SignOutAsync(Authentications.AuthenticationScheme);

            return RedirectToPage("/Index");
        }
    }
}