﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace Accelist.Renegade.Web.APIs
{
    [Route("api/v1/store")]
    public class StoreController : Controller
    {
        [HttpGet]
        public ActionResult<string> Get()
        {
            return Ok();
        }
    }
}
