﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Accelist.Renegade.Services;
using Accelist.Renegade.Services.Models.Libraries;
using Microsoft.AspNetCore.Mvc;

namespace Accelist.Renegade.Web.APIs
{
    [Route("api/v1/library")]
    public class LibraryController : Controller
    {
        private readonly VideoGameService VideoGameService;

        public LibraryController(VideoGameService videoGameService)
        {
            this.VideoGameService = videoGameService;
        }

        [HttpGet("my-games", Name = "GetMyGames")]
        public async Task<ActionResult<List<UserGameModel>>> GetMyGames()
        {
            var username = this
                .User
                .Claims
                .Where(Q => Q.Type == ClaimTypes.Name)
                .Select(Q => Q.Value)
                .FirstOrDefault();

            var games = await this.VideoGameService.GetUserGames(username);

            return Ok(games);
        }
    }
}
