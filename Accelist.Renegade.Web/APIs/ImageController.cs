﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Accelist.Renegade.Services;
using Microsoft.AspNetCore.Mvc;

namespace Accelist.Renegade.Web.APIs
{
    [Route("api/v1/image")]
    public class ImageController : Controller
    {
        private readonly ImageService ImageService;

        public ImageController(ImageService imageService)
        {
            this.ImageService = imageService;
        }

        [HttpGet("{id}", Name = "GetImage")]
        public async Task<ActionResult> Get(Guid id)
        {
            //var image = await this.ImageService.GetMiniOImage(id);

            //return File(image, "image/png", $"{ id }.png");

            var url = await this.ImageService.GetMiniOImage(id);

            return Redirect(url);
        }
    }
}
