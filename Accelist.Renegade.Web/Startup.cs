using Accelist.Renegade.Entities;
using Accelist.Renegade.Services;
using Accelist.Renegade.Services.Constants;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Minio;

namespace Accelist.Renegade.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            services.AddSwaggerGen(config =>
            {
                config.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = "Accelist Renegade API",
                    Version = "v1"
                });
            });

            services.AddDbContextPool<RenegadeDbContext>(options =>
            {
                var connectionString = this.Configuration.GetConnectionString(ConnectionStringName.RenegadeDb);
                options.UseSqlServer(connectionString);

                options.ConfigureWarnings(warnings =>
                {
                    warnings.Throw(RelationalEventId.QueryClientEvaluationWarning);
                });
            });

            services.AddAuthentication(Authentications.AuthenticationScheme)
                .AddCookie(Authentications.AuthenticationScheme, options =>
                {
                    options.LoginPath = "/auth/login";
                    options.LogoutPath = "/auth/logout";
                    options.AccessDeniedPath = "/denied";
                    options.ExpireTimeSpan = new System.TimeSpan(90, 0, 0, 0);
                });

            services.AddTransient<ImageService>();
            services.AddTransient<AuthenticationService>();
            services.AddTransient<VideoGameService>();

            services.AddSingleton<MinioClient>(di =>
            {
                var url = this.Configuration["MinIO:Url"];
                var accessKey = this.Configuration["MinIO:AccessKey"];
                var secretKey = this.Configuration["MinIO:SecretKey"];
                return new MinioClient(url, accessKey, secretKey);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();

            app.UseAuthentication();

            app.UseSwagger();
            app.UseSwaggerUI(config =>
            {
                config.SwaggerEndpoint("/swagger/v1/swagger.json", "Accelist Renegade API V1");
            });

            app.UseMvc();
        }
    }
}
