# Depending on the operating system of the host machines(s) that will build or run the containers, the image specified in the FROM statement may need to be changed.
# For more information, please see https://aka.ms/containercompat

# Install Node.js.
FROM node:10-alpine AS instapack
RUN npm install -g instapack@7.2.7
COPY ./Accelist.Renegade.Web /src/Accelist.Renegade.Web
WORKDIR /src/Accelist.Renegade.Web
RUN ipack

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2-alpine AS base
WORKDIR /app

# Copy csproj and restore as distinct layers.
FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build
COPY --from=instapack /src /src
WORKDIR /src
COPY Accelist.Renegade.Web/Accelist.Renegade.Web.csproj ./Accelist.Renegade.Web/
COPY Accelist.Renegade.Services/Accelist.Renegade.Services.csproj ./Accelist.Renegade.Services/
COPY Accelist.Renegade.Entities/Accelist.Renegade.Entities.csproj ./Accelist.Renegade.Entities/

RUN dotnet restore Accelist.Renegade.Web/Accelist.Renegade.Web.csproj

COPY . ./
WORKDIR /src/Accelist.Renegade.Web
RUN dotnet build "Accelist.Renegade.Web.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "Accelist.Renegade.Web.csproj" -c Release -o /app

# Build runtime image.
FROM base as final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "Accelist.Renegade.Web.dll"]
